from decimal import *

def ex3(start, stop, step=0.5):
    list = []
    while start <= stop:
        list.append(Decimal(start))
        start+= step
    return list

print(ex3(2, 5.5))
