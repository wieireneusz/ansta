def ex2(a_list, n):
    n_list = [ i for i in range(1, n+1)]
    return(list(set(n_list)-set(a_list)))

print(ex2([2,3,7,4,9], 10))