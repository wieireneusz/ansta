def ex1(start, end):
    start = int(start.replace('-', ''))
    end = int(end.replace('-', ''))

    zips_list = []

    for zipcode in range(start, end+1):
        zipcode_str = str(zipcode)
        if len(zipcode_str) < 5:
            zipcode_str = ('0' * (5 - len(zipcode_str))) + zipcode_str
        zipcode_str = zipcode_str[:2] + '-' + zipcode_str[2:]
        zips_list.append(zipcode_str)

    return zips_list

print(ex1('79-900', '80-155'))
